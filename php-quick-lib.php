<?php
/**
 * PHP quick library functions
 *
 * A short procedural library to help write PHP scripts quickly
 *
 * Sources at <http://github.com/piwi/php-quick-lib>
 * Copyright (c) 2014-2015 Pierre Cassat <http://e-piwi.fr/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package     php-quick-lib
 * @prefix      phpqlib_|PHPQLIB_
 * @author      Pierre Cassat <http://e-piwi.fr/>
 * @license     Apache 2.0
 * @link        http://github.com/piwi/php-quick-lib Sources & bug reports
 * @link        http://docs.ateliers-pierrot.fr/php-quick-lib/ Online documentation
 * @example     TUTORIAL.md
 * @filesource
 */

/**
 * The library name
 *
 * @package     php-quick-lib-internals
 */
define('PHPQLIB_NAME', 'PHPQuickLibrary');

/**
 * The library short name
 *
 * @package     php-quick-lib-internals
 */
define('PHPQLIB_SHORTKEY', 'phpqlib');

/**
 * The library version number
 *
 * @version
 * @package     php-quick-lib-internals
 */
define('PHPQLIB_VERSION', '1.0.0');

/**
 * The library sources repository URL
 *
 * @package     php-quick-lib-internals
 */
define('PHPQLIB_SOURCES', 'http://github.com/piwi/php-quick-lib');


// --------------------------------------------
// Callback methods
// --------------------------------------------

/**
 * Apply a callback on arguments
 *
 * @param   callable    $callback   A callback function to evaluate
 * @return  mixed
 */
function callback($callback)
{
    $args = func_get_args();
    array_shift($args);
    if (empty($callback)) {
        return (count($args)==1 ? array_shift($args) : $args);
    }
    if (is_callable($callback)) {
        try { $result = call_user_func_array($callback, $args); } catch (Exception $e) { $result = $e->getMessage(); }
    } else {
        $result = $callback;
    }
    return $result;
}

// --------------------------------------------
// Cache methods
// --------------------------------------------

/**
 * Get the path of a cache file or directory
 *
 * @param   string  $file_name
 * @return  string
 */
function get_cache_path($file_name)
{
    $file_path  = realpath($file_name);
    $tmp_dir    = get_temp_dir();
    if (empty($file_path) || strpos($file_path, $tmp_dir) || !file_exists($file_path)) {
        $file_path = get_path($tmp_dir . '/' . str_replace($tmp_dir, '', $file_name));
    }
    return $file_path;
}

/**
 * Test if a file is already cached and if it is still valid
 *
 * @param   string      $file_name
 * @param   int|null    $valid      A timestamp to test cached version validity
 * @return  bool
 */
function is_cached($file_name, $valid = null)
{
    $file_path = get_cache_path($file_name);
    return (bool) (
        file_exists($file_path) && (
            empty($valid) || $valid<filemtime($file_path)
        )
    );
}

/**
 * Get a cache content
 *
 * @param   string  $file_name
 * @return  null|string
 */
function get_cache($file_name)
{
    $file_path = get_cache_path($file_name);
    if (file_exists($file_path)) {
        return file_get_contents($file_path);
    }
    return null;
}

/**
 * Set a cache content
 *
 * @param   string  $file_name
 * @param   string|array|callable  $data
 * @return  bool
 */
function set_cache($file_name, $data)
{
    $file_path = get_cache_path($file_name);
    if (is_array($data)) {
        $data = implode(PHP_EOL, $data);
    } elseif (is_callable($data)) {
        $data = callback($data);
    }
    return (bool) file_put_contents($file_path, (string) $data);
}

/**
 * Delete a cache file or the entire cache directory
 *
 * @param   string|null $file_name
 * @return  bool
 */
function flush_cache($file_name = null)
{
    if (is_null($file_name)) {
        return remove_dir(get_temp_dir());
    } else {
        $file_path = get_cache_path($file_name);
        return unlink($file_path);
    }
}

// --------------------------------------------
// CLI options management
// --------------------------------------------

/**
 * Quick treatment of command line parameters
 *
 * Using the last parameter `$full_opts` allows to define each option like:
 *
 *     array(
 *          // short and/or long option name, with `:` or `::` suffix if needed
 *         'options'   => array( 'L', 'long'),
 *          // final name of the setting to define, it defaults to `value` if it is a string
 *         'name'      => final setting name,
 *          // the name of the setting to set or a closure to transform parameter argument
 *         'value'     => string to set or closure like "function($value){ return $value.'suffix'; }",
 *          // a validation method that must return `true` for a valid argument
 *         'validate'  => closure that returns a boolean like "function($value){ return is_string($value); }",
 *          // a default value for options with optional arguments and no command line argument
 *         'default'   => default value,
 *     )
 *
 * @param   array   $short_opts Array of short options like `option_letter[:[:]] => setting_name or closure`
 * @param   array   $long_opts  Array of long options like `option_name[:[:]] => setting_name or closure`
 * @param   array   $full_opts  Array of full options definitions
 * @return  array
 * @throws  Exception if a validation fails
 * @setting argv                Internal PHP `$argv`
 * @link    http://php.net/manual/en/features.commandline.php "Using PHP CLI" on PHP Manual
 * @api
 */
function get_options($short_opts = array(), $long_opts = array(), $full_opts = null)
{
    $short_opts_keys    = '';
    $long_opts_keys     = array();
    if (empty($full_opts)) {
        $full_opts = array();
        if (!empty($short_opts)) {
            foreach ($short_opts as $k=>$v) {
                $short_opts_keys .= $k;
                $full_opts[] = array(
                    'options'   => array($k),
                    'name'      => is_string($v) ? $v : $k,
                    'value'     => $v
                );
                $short_opts[$k] = count($full_opts) - 1;
            }
        }
        if (!empty($long_opts)) {
            foreach ($long_opts as $k=>$v) {
                $long_opts_keys[] = $k;
                $full_opts[] = array(
                    'options'   => array($k),
                    'name'      => is_string($v) ? $v : $k,
                    'value'     => $v
                );
                $long_opts[$k] = count($full_opts) - 1;
            }
        }
    } else {
        $short_opts = array();
        $long_opts  = array();
        foreach ($full_opts as $i=>$item) {
            foreach ($item['options'] as $opt) {
                if (strlen(str_replace(':', '', $opt)) == 1) {
                    $short_opts[$opt] = $i;
                    $short_opts_keys .= $opt;
                } else {
                    $long_opts[$opt] = $i;
                    $long_opts_keys[] = $opt;
                }
            }
            if (empty($item['name']) && !empty($item['value']) && is_string($item['value'])) {
                $full_opts[$i]['name'] = $item['value'];
            }
        }
    }
/*/
debug($short_opts);
debug($long_opts);
debug($full_opts);
debug($short_opts_keys);
debug($long_opts_keys);
//*/
    $options = getopt($short_opts_keys, $long_opts_keys);
    $pruneargv = array();
    $argv = settings('argv');
    $default = function($v) { return ($v===false ? true : $v); };
    foreach ($options as $n=>$v) {
/*/
debug('> entering option treatment with '.$n.' => '.var_export($v,1));
//*/
        $key = null;
        if (is_array($v)) {
            $v = $v[0];
        }
        if (strlen($n)==1 && (
            array_key_exists($n, $short_opts) ||
            array_key_exists($n.':', $short_opts) ||
            array_key_exists($n.'::', $short_opts)
        )) {
            $key = $short_opts[
                array_key_exists($n.'::', $short_opts) ? $n.'::' : (
                    array_key_exists($n.':', $short_opts) ? $n.':' : $n
                )
            ];
        } elseif (
            array_key_exists($n, $long_opts) ||
            array_key_exists($n.':', $long_opts) ||
            array_key_exists($n.'::', $long_opts)
        ) {
            $key = $long_opts[
                array_key_exists($n.'::', $long_opts) ? $n.'::' : (
                    array_key_exists($n.':', $long_opts) ? $n.':' : $n
                )
            ];
        }
        if (!is_null($key)) {
            $name   = isset($full_opts[$key]['name']) ? $full_opts[$key]['name'] : $n;
            $value  = isset($full_opts[$key]['value']) ? $full_opts[$key]['value'] : $key;
            $result = callback(is_string($value) ? $default : $value, $v);
            if (isset($full_opts[$key]['validate']) && callback($full_opts[$key]['validate'], $v)!==true) {
                throw new Exception(sprintf('Option value error for parameter "%s"!', $name));
            }
            if (substr_count($full_opts[$key]['options'][0], ':')==2 &&
                (empty($v) || $v===false) &&
                isset($full_opts[$key]['default'])
            ) {
                $result = $full_opts[$key]['default'];
            }
/*/
debug('> treating option '.$name.' setting value '.$result);
//*/
            settings($name, $result);
        }
        foreach ($argv as $key => $chunk) {
            if ($key>0) {
                $regex = '/^'. (strlen($n)>1 ? '--' : '-') . $n . '/';
                if (($chunk == $v && $argv[$key-1][0] == '-') || preg_match($regex, $chunk)) {
                    array_push($pruneargv, $key);
                }
            }
        }
    }
    while ($key = array_pop($pruneargv)) {
        unset($argv[$key]);
    }
    settings('argv', $argv);
    return $options;
}

/**
 * Default help string
 *
 * @package     php-quick-lib-internals
 * @return      void
 */
function default_help_string()
{
    output("Use option '--help' to get help.".PHP_EOL);
}

/**
 * Default usage string
 *
 * @package     php-quick-lib-internals
 * @return      void
 */
function default_usage()
{
    extract(settings());
    $howto  = default_cli_usage_info();
    $ctt    = <<<EOT

usage:  {$argv[0]} [--help | -h] [--debug | -x]

options:
    -h | --help       get help
    -x | --debug      debug current settings value

{$howto}


EOT;
    output($ctt);
}

/**
 * Common cli usage how-to
 *
 * @package     php-quick-lib-internals
 * @return      string
 */
function default_cli_usage_info()
{
    return 'Command line options usage reminder:
    - you can group short options like `-ab`
    - options with required argument DOES NOT require the equal sign `--opt "value"`
    - options with optional argument DOES require the equal sign `--opt="value"`';
}

/**
 * Get the default options definitions table (command line arguments)
 *
 * Default options are:
 *  - "-h" or "--help" to see the "usage()" string if it exists
 *  - "-x" or "--debug" to debug current settings
 *
 * @package     php-quick-lib-internals
 * @return      array
 */
function default_options_definitions()
{
    return array(
        array(
            'options'   => array('V', 'version'),
            'value'     => function() { php_quick_lib_info(); exit(); },
        ),
        array(
            'options'   => array('h', 'help'),
            'value'     => function() {
                if (function_exists('usage')) { usage(); exit(0); }
                else { default_usage(); exit(0); }
            },
        ),
        array(
            'options'   => array('x', 'debug'),
            'value'     => function() { debug(settings()); exit(); },
        )
    );
}

/**
 * Parse the default options (command line arguments)
 *
 * @package     php-quick-lib-internals
 * @see         default_options_definitions()
 * @return      array
 */
function default_get_options()
{
    $args = get_options(null, null, default_options_definitions());
    if (($piped = read_safe_stdin()) && !empty($piped)) {
        $args['piped'] = $piped;
    }
    return $args;
}

// --------------------------------------------
// Debugging methods
// --------------------------------------------

/**
 * Debug all arguments in plain text and exit
 *
 * @return  void
 * @see     debug()
 */
function hard_debug()
{
    if (!headers_sent()) {
        header('Content-Type: text/plain');
    }
    call_user_func_array('debug', func_get_args());
    exit('-- debug --');
}

/**
 * Debug anything on output (special "dump")
 *
 * @return  void
 * @see     visual_dump()
 */
function debug()
{
    foreach (func_get_args() as $i=>$j) {
        echo visual_dump($j);
    }
}

/**
 * Construct a visual dump of some values
 *
 * @param   mixed   $values
 * @param   int     $padd   Internal padder value
 * @return  string
 * @setting new_line        New line character
 */
function visual_dump($values, $padd = 0)
{
    $str = '';
    if (is_array($values)) {
        $maxlength=0;
        foreach ($values as $var=>$val) if (strlen($var)>$maxlength) $maxlength = strlen($var);
        foreach ($values as $var=>$val) {
            $str .= str_pad(' ', $padd);
            if (!is_numeric($var)) $str .= str_pad($var, $maxlength)."  : ";
            if (is_array($val)) {
                $type = 'string';
                foreach ($val as $v=>$k) if (is_array($k)) $type = 'array';
                if ($type==='array') {
                    $str .= '[]'.settings('new_line');
                    $str .= visual_dump($val, ($padd+$maxlength+(is_numeric($var) ? 0 : 5)));
                } else {
                    $str .= '[ '.join(' , ', $val).' ]'.settings('new_line');
                }
            } elseif (is_object($val) && !method_exists($val, '__toString')) {
                if (method_exists($val, '__set_state')) {
                    $str .= var_export($val,1);
                } else {
                    ob_start();
                    var_dump($val);
                    $str .=  ob_get_contents();
                    ob_end_clean();
                }
                $str .= settings('new_line');
            } else {
                $str .= (string) $val;
                $str .= settings('new_line');
            }
        }
    } else {
        $str .= (string) $values;
        $str .= settings('new_line');
    }
    return $str;
}

// --------------------------------------------
// Emails methods
// --------------------------------------------

/**
 * Validate an email address
 *
 * @param   string  $email The string to validate
 * @return  bool    Returns `true` if this is an email
 */
function is_email($email = null)
{
    if (is_null($email) || !$email || !is_string($email)) {
        return false;
    }
    return (bool) (preg_match('/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/', $email) > 0);
}

/**
 * @param   string  $to
 * @param   string  $subject
 * @param   string  $message
 * @param   string  $additional_headers
 * @param   string  $additional_parameters
 * @return  bool
 * @throws  InvalidArgumentException if the receiver si not an email
 */
function send_email($to, $subject, $message, $additional_headers = '', $additional_parameters = '')
{
    if (!is_email($to)) {
        throw new \InvalidArgumentException(
            sprintf('Email receiver is not an email ("%s")!', $to)
        );
    }
    return mail(
        $to, safe_string($subject), safe_string($message),
        safe_string($additional_headers), safe_string($additional_parameters)
    );
}

// --------------------------------------------
// File-system methods
// --------------------------------------------

/**
 * Make a temporary directory in the system temporary filesystem
 *
 * @param   string  $name
 * @return  string
 */
function get_temp_dir($name = null)
{
    $opt = settings('temporary_directory');
    if (!empty($opt) && file_exists($opt) && is_dir($opt) && is_writable($opt)) {
        return $opt;
    }
    $basepath = get_path(sys_get_temp_dir().'/'.PHPQLIB_SHORTKEY);
    if (file_exists($basepath) && !is_dir($basepath)) {
        unlink($basepath);
    }
    if (is_null($name)) {
        $app_name = settings('app_name');
        if (!is_null($app_name)) {
            $name = $app_name;
        }
    }
    $d = !empty($name) ? get_path($basepath.'/'.$name) : $basepath;
    if (file_exists($d) && !is_dir($d)) {
        unlink($d);
    }
    if (!file_exists($d)) {
        mkdir($d, 0775, true);
    }
    settings('temporary_directory', $d);
    return (is_dir($d) ? $d : null);
}

/**
 * Remove a directory and its content
 *
 * @param   string  $dir
 * @return  bool
 */
function remove_dir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if (!in_array($object, array('.', '..'))) {
                if (filetype($dir.'/'.$object) == 'dir') {
                    remove_dir($dir.'/'.$object);
                } else {
                    unlink($dir.'/'.$object);
                }
            }
        }
        reset($objects);
        return rmdir($dir);
    }
    return false;
}

// --------------------------------------------
// Handlers methods
// --------------------------------------------

/**
 * Exception handling
 *
 * To use this handler, write:
 *
 *      set_exception_handler('exception_handler');
 *
 * @param   mixed   $e      string or Exception
 * @param   int     $status final exit status
 * @return  void
 */
function exception_handler($e = null, $status = 1)
{
    // messages
    $_trace = (is_object($e) && ($e instanceof Exception) ? $e->getTrace() : debug_backtrace());
    $_last  = (isset($_trace[1]) && is_array($_trace[1]) ? $_trace[1] : (isset($_trace[0]) && is_array($_trace[0]) ? $_trace[0] : null));
    $_info = array(
        'message'       => (is_object($e) && ($e instanceof Exception) ? "Caught '".get_class($e)."' with message '" . $e->getMessage() . "'" : (string) $e),
        'file_line'     => (is_object($e) && ($e instanceof Exception) ? "thrown in file " . $e->getFile() . " on line " . $e->getLine() : ''),
        'last_file'     => (!is_null($_last) ? "in " . $_last['file'].'::'.$_last['line'].'::'.$_last['function'].'()' : ''),
        'stack_trace'   => $_trace,
    );
    // log the error
    error_log('PHP Fatal error: '.$_info['message'].' '.$_info['last_file'].' '.$_info['file_line']);
    // inform user
    error($_info['message'] . ' ' . $_info['file_line'], $status);
}

/**
 * Error handler to use `exception()` for all errors
 *
 * To use this handler, write:
 *
 *      set_error_handler('error_handler');
 *
 * @param   int     $errno
 * @param   string  $errstr
 * @param   string  $errfile
 * @param   int     $errline
 * @param   array   $errcontext
 * @return  bool    true to stop error propagation
 * @throws  ErrorException created with current error info
 * @see     exception_handler()
 */
function error_handler($errno, $errstr, $errfile, $errline, $errcontext = null)
{
    if (!(error_reporting() & $errno)) {
        return true;
    }
    exception_handler(new ErrorException( $errstr, 0, $errno, $errfile, $errline ));
    return true;
}

/**
 * Checks for a fatal error on shutdown
 *
 * To use this handler, write:
 *
 *      register_shutdown_function('shutdown_handler');
 *
 * @param   int     $type
 * @see     error_handler()
 */
function shutdown_handler($type = E_ERROR)
{
    $error = error_get_last();
    if ($error["type"] & $type) {
        error_handler( $error["type"], $error["message"], $error["file"], $error["line"] );
    }
}

/**
 * Force a 'content-type' header if it is not sent
 *
 * To use it, write:
 *
 *      header_register_callback('header_handler');
 *
 * @return void
 * @setting content-type
 * @setting encoding
 */
function header_handler()
{
    $toadd = true;
    foreach (headers_list() as $header) {
        if (strpos(strtolower($header), 'content-type:')) {
            $toadd = false;
        }
    }
    if ($toadd) {
        header('Content-Type: ' . settings('content-type') . '; charset: ' . settings('encoding'));
    }
}

// --------------------------------------------
// Input methods
// --------------------------------------------

/**
 * Prompt user for an input and (optionally) validate its response
 *
 * The `$validate` argument must be a closure returning a boolean:
 *
 *      $validate = function ($value) { return ($value === 'ok'); }
 *
 * @param   string     $str
 * @param   mixed      $default
 * @param   callable   $validate
 * @param   int        $timeout
 * @return  string
 * @api
 */
function user_prompt($str, $default = null, $validate = null, $timeout = 0)
{
    output(rtrim($str, ' '.PHP_EOL).' ');
    $handlers = array(
        0 => fopen('php://stdin','r'),
        1 => fopen('php://stdout','w'),
        2 => fopen('php://stderr','a')
    );
    $read = $handlers;
    if ($timeout>0) {
        $write  = $except = array();
    } else {
        $write  = $handlers;
        $except = $handlers;
    }
    $line       = null;
    if ($result = stream_select($read, $write, $except, $timeout)) {
        if ($result !== false && $result > 0) {
            $hdl    = count($write)>0 ? $write : $read;
            $key    = array_search($hdl[0], $handlers);
            $line   = rtrim(fgets($handlers[$key]), PHP_EOL);
        }
    }
    foreach ($handlers as $pipe) {
        @fclose($pipe);
    }
    if (empty($line) && $default) {
        $line   = $default;
    }
    if ($validate && callback($validate, $line)!==true) {
        output('_ abort' . PHP_EOL);
        exit(0);
    }
    return $line;
}

/**
 * Get any output from previous command piped from STDIN
 *
 * @return  string|null
 * @api
 */
function read_safe_stdin()
{
    $data   = '';
    $read   = array(STDIN);
    $write  = array();
    $except = array();
    try {
        $result = stream_select($read, $write, $except, 0);
        if ($result !== false && $result > 0) {
            while (!feof(STDIN)) {
                $data .= fgets(STDIN);
            }
        }
        @file_put_contents(STDIN, '');
    } catch (Exception $e) {
        $data = null;
    }
    return $data;
}

// --------------------------------------------
// Logging methods
// --------------------------------------------

/**
 * Create a logfile if it does not exist and open it and return file name
 *
 * This will load as settings:
 *
 *      logfile => the name of the logfile
 *      logger  => the resource to write in the log file
 *
 * @param   string     $name
 * @param   string     $prefix
 * @return  string
 */
function create_logfile($name = null, $prefix = PHPQLIB_SHORTKEY)
{
    if (empty($name)) {
        $name = $prefix.'-'.date('dmy').'.log';
    }
    if (
        file_exists($name) && !empty($logfile) && realpath($name)==$logfile &&
        false !== ($log = fopen(realpath($name), 'a+'))
    ) {
        $logfile    = $name;
    } elseif (false !== ($log = fopen($name, 'a+'))) {
        $logfile    = realpath(__DIR__ . DIRECTORY_SEPARATOR . $name);
    } else {
        $logfile    = tempnam(sys_get_temp_dir(), $name);
        $log        = fopen($logfile, 'a+');
    }
    settings('logfile', $logfile);
    settings('logger', $log);
    return $logfile;
}

/**
 * Write a new info in a log file (current one by default)
 *
 * @param   string             $str
 * @param   string             $type       Used to prefix the info
 * @param   string|resource    $logfile
 * @return  bool
 * @api
 */
function logger($str, $type = null, $logfile = null)
{
    if (empty($logfile)) {
        $logger = settings('logger');
    } else {
        $known_logfile = settings('logfile');
        if (is_resource($logfile)) {
            $logger = $logfile;
        } elseif (!empty($known_logfile) && $logfile === $known_logfile) {
            $logger = settings('logger');
        } elseif (create_logfile($logfile)) {
            $logger = settings('logger');
        } else {
            return null;
        }
    }
    $info = '['.date('d/m/y H:i:s').' - '.@getmypid().']'
            .(!empty($type) ? ' '.strtoupper($type).' - ' : ' ')
            .$str
            .PHP_EOL;
    return (false !== fwrite($logger, $info));
}

// --------------------------------------------
// Output methods
// --------------------------------------------

/**
 * Write a string
 *
 * This will write the string to STDOUT (status=0) or STDERR (status>0) in CLI mode
 * or on screen otherwise.
 *
 * @param   string  $str
 * @param   int     $status
 * @return  void
 * @api
 */
function output($str, $status = 0)
{
    if (IS_CLI) {
        if (false === fwrite( ($status===0 ? STDOUT : STDERR), $str)) {
            echo $str;
        }
    } else {
        echo $str.settings('new_line');
    }
}

/**
 * Simple error handling
 *
 * This will write an error info and exit with an error status.
 *
 * The message is formed like `ERROR > msg`.
 *
 * @param   string  $str      Error string
 * @param   int     $status   The final exit status
 * @return  void
 * @see     help_string()
 * @see     default_help_string()
 * @api
 */
function error($str, $status = 1)
{
    output("ERROR > " . $str . settings('new_line'), $status);
    if (IS_CLI) {
        if (function_exists('help_string')) {
            help_string();
        } else {
            default_help_string();
        }
    }
    exit($status);
}

/**
 * Simple success info handling
 *
 * The message is formed like `OK > msg`.
 *
 * @param   string   $str      Information string
 * @param   bool     $exit     Exit or not after the info (default is `false`)
 * @return  void
 * @api
 */
function ok($str, $exit = true)
{
    output("OK > " . $str . settings('new_line'));
    if ($exit) {
        exit();
    }
}

/**
 * Simple info handling
 *
 * The message is formed like `> msg`.
 *
 * @param   string   $str      Information string
 * @return  void
 * @api
 */
function info($str)
{
    output("> " . $str . settings('new_line'));
}

/**
 * Update last line or characters of the output (CLI only)
 *
 * Note that this will not end the line (no new line is passed except `$end = true`).
 *
 * @param   string  $str
 * @param   int     $length
 * @param   bool    $end
 * @return  void
 * @api
 */
function update_output($str, $length = 0, $end = false)
{
    if (!IS_CLI) return;
    output(
         ($length>0 ? str_repeat("\x08", $length) : "\r")
        .$str
        .($end===true ? PHP_EOL : '')
    );
}

// --------------------------------------------
// Paths methods
// --------------------------------------------

/**
 * Get a sanitized path
 *
 * @param   string  $path
 * @return  string
 */
function get_path($path)
{
    return str_replace('/', DIRECTORY_SEPARATOR, rtrim(
        str_replace('//', '/', $path), '/'.DIRECTORY_SEPARATOR));
}

/**
 * Get a "secured" path replacing base path by '[***]'
 *
 * @param   string  $path
 * @return  string
 */
function get_secured_path($path)
{
    $parts = explode('/', realpath('.'));
    array_pop($parts);
    array_pop($parts);
    return str_replace(join('/', $parts), '/[***]', $path);
}

/**
 * Get a relative web path of a file from `settings('web_base_path')`
 *
 * @param   string  $path
 * @return  string
 */
function get_web_path($path)
{
    $protocols = array('http://', 'https://', '//');
    foreach ($protocols as $protocol) {
        if (substr($path, 0, strlen($protocol))==$protocol) return $path;
    }
    $base = settings('web_base_path');
    if (empty($base)) $base = settings('cwd');
    return $base.$path;
}

// --------------------------------------------
// Processes methods
// --------------------------------------------

/**
 * Execute a simple command and returns the result and final status
 *
 * If the `$logfile` argument is defined, the final result will be logged
 * in that file AND returns.
 *
 * @param   string  $cmd
 * @param   string  $logfile
 * @param   bool    $on_background
 * @return  array   An array like ( output lines (array) , status (int) )
 * @api
 */
function execute($cmd, $logfile = null, $on_background = false)
{
    unset($output);
    if ($on_background) {
        $cmd .= ' &';
    }
    exec($cmd, $output, $status);
    if (!empty($logfile)) {
        logger(join(PHP_EOL, $output), null, $logfile);
    }
    return array($output, $status);
}

/**
 * Run a command on a Linux/UNIX system
 *
 * Accepts a shell command to run
 *
 * @param   string  $command    The command to run
 * @param   string  $path       The path to go to
 * @param   array   $opts       An array of optional environment arguments
 * @return  array   An array like ( result , status , error )
 * @throws  Exception caught during execution
 * @api
 */
function process($command, $path = '.', $opts = array())
{
    try {
        list($proc, $pipes) = process_prepare($command, $path, $opts);
        return process_close($proc, $pipes);
    } catch(Exception $e) {
        throw $e;
    }
}

/**
 * Run a command on a Linux/UNIX system and return I/O streams
 *
 * Accepts a shell command to run
 *
 * @param   string  $cmd    The command to run
 * @param   string  $path   The path to go to
 * @param   array   $env    An array of optional environment arguments
 * @return  array   An array like ( proc , pipes )
 * @throws  Exception if the command can not be started
 */
function process_prepare($cmd, $path = '.', $env = array())
{
    $descs = array(
        0 => array('pipe', 'r+'),   // stdin
        1 => array('pipe', 'w+'),   // stdout
        2 => array('pipe', 'w+'),   // stderr
    );
    $process = proc_open($cmd, $descs, $pipes, $path, $env);
    if (is_resource($process)) {
        stream_set_blocking($pipes[0], 0);
//        stream_set_blocking($pipes[1], 0);
        stream_set_blocking($pipes[2], 0);
        return array(&$process, &$pipes);
    } else {
        throw new Exception(sprintf('The command "%s" can not be started!', $cmd));
    }
}

/**
 * Run a command on a Linux/UNIX system
 *
 * Accepts a shell command to run
 *
 * @param   resource  $process    The process to close
 * @param   array     $pipes      An array of pipes like ( stdin , stdout , stderr )
 * @return  array     An array like ( result , status , error )
 * @throws  Exception if the command can not be started
 */
function process_close($process, $pipes = array())
{
    if (is_resource($process)) {
        if (is_resource($pipes[0])) {
            @fclose($pipes[0]);
        }
        $stdout = isset($pipes[1]) && is_resource($pipes[1]) ? stream_get_contents($pipes[1]) : null;
        if (is_resource($pipes[1])) {
            @fclose($pipes[1]);
        }
        $stderr = isset($pipes[2]) && is_resource($pipes[2]) ? stream_get_contents($pipes[2]) : null;
        if (is_resource($pipes[2])) {
            @fclose($pipes[2]);
        }
        $return = proc_close($process);
        return array(
            rtrim($stdout, PHP_EOL),
            trim($return),
            rtrim($stderr, PHP_EOL)
        );
    }
}

// --------------------------------------------
// Settings methods
// --------------------------------------------

/**
 * Config settings management
 *
 * Usage
 *
 * - no arg : get the settings table as an array
 * - 1 arg:
 *      - array : merge settings with arg
 *      - string : get the $var setting ($default by default)
 * - 2 args: (re)define $var on $val
 *
 * A classic usage in a PHP script should be:
 *
 *      // this will set the `opt` setting on value `val`
 *      settings(opt, val);
 *
 *      // this will load the `opt` setting current value in $val
 *      $val = settings(opt);
 *      $val = settings(opt, null, default); // with a default value
 *
 *      // this will load all current settings as env vars
 *      extract(settings());
 *
 * The `$val` can be a closure evaluated on define. To get the original closure, use:
 *
 *      settings(opt, function($t=0){ return $t + 1; });
 *      echo settings(opt); // = 1
 *      $fct = settings(opt_closure);
 *      echo $fct(10); // = 11
 *
 * @param   string|array   $var
 * @param   mixed          $val
 * @param   mixed          $default
 * @return  null|array|mixed
 * @see     ::callback()
 * @api
 */
function settings($var = null, $val = null, $default = null)
{
    /** @var array The global table of environment settings */
    static $settings = array();
    if (is_null($var)) {
        $return = $settings;
    } elseif (is_null($val)) {
        if (is_array($var)) {
            foreach ($var as $k=>$v) {
                settings($k, $v);
            }
            $return = true;
        } else {
            $return = isset($settings[$var]) ? $settings[$var] : $default;
        }
    } else {
        if (is_object($val) && ($val instanceof Closure)) {
            $settings[$var.'_closure'] = $val;
            $settings[$var] = callback($val);
        } else {
            $settings[$var] = $val;
        }
        $return = true;
    }
    return $return;
}

/**
 * Set or get a unique ID identified by `$name`
 *
 * @param   string  $name   The name of the ID to get
 * @param   bool    $reset  Force creation of a new ID
 * @return  string
 */
function get_id($name, $reset = false)
{
    $ids = settings('ids');
    if (is_null($ids)) {
        $ids = array();
    }
    if (!array_key_exists($name, $ids) || $reset===true) {
        $id = slugify($name);
        if (in_array($id, $ids)) $id .= uniqid();
        $ids[$name] = $id;
    }
    settings('ids', $ids);
    return $ids[$name];
}

/**
 * @param   string  $path
 * @param   null    $type
 * @return  array|mixed
 * @throws  ErrorException
 * @throws  InvalidArgumentException
 */
function load_config($path, $type = null)
{
    if (!file_exists($path)) {
        throw new ErrorException(
            sprintf('Config file "%s" not found!', $path)
        );
    }
    if (is_null($type)) {
        if (strtolower(substr($path, -4))=='json') {
            $type = 'json';
        } elseif (strtolower(substr($path, -3))=='ini') {
            $type = 'ini';
        } elseif (strtolower(substr($path, -3))=='php') {
            $type = 'php';
        }
    }
    $config = array();
    switch ($type) {
        case 'ini':
            $config = parse_ini_file($path, true);
            break;
        case 'json':
            $config = json_decode(file_get_contents($path), true);
            break;
        case 'php':
            $config = array();
            include $path;
            break;
        default:
            throw new InvalidArgumentException(
                sprintf('Unknown config file type "%s"!', $path)
            );
            break;
    }
    return $config;
}

// --------------------------------------------
// Strings methods
// --------------------------------------------

/**
 * Returns a safe string passing it in `$mask` with `sprintf()` and to a list of optional callbacks
 *
 * @param    mixed          $what        The original content to stringify
 * @param    string         $mask        The mask to use to build the content
 * @param    null|callable  $callback    A callback method to finally transform the string
 * @param    string         $items_glue  A string used to join array items
 * @setting  mask_default                Default value for `$mask`
 * @setting  date_format                 String used to format DateTime objects
 * @return   string
 */
function safe_string($what, $mask = '%s', $callback = null, $items_glue = ' ')
{
    $str = '';
    if (is_array($what)) {
        $count = 0;
        foreach ($what as $var=>$val) {
            $valnum = substr_count($mask, '%s');
            $str .= ($count>0 ? $items_glue : '').(($valnum > 1) ? sprintf($mask, $var, $val) : sprintf($mask, $val));
            $count++;
        }
    } elseif (is_object($what)) {
        if ($what instanceof DateTime) {
            $str .= sprintf($mask, $what->format(settings('date_format')));
        } elseif ($what instanceof Closure) {
            $str .= sprintf($mask, callback($what, $str));
        }
    } elseif (is_string($what) || is_numeric($what)) {
        $str .= sprintf($mask, (string) $what);
    }
    $str = callback($callback, $str);
    return $str;
}

/**
 * Get a readable string from any original string
 *
 * @param   string          $str        The original string
 * @param   null|callable   $callback   A callback method to finally transform the string
 * @setting string_spacify              Car(s). replaced by a space
 * @setting string_strip                Car(s). stripped
 * @return  string
 */
function stringify($str, $callback = null)
{
    $str = safe_string($str);
    $str = str_replace(settings('string_spacify'), ' ', (string) $str);
    $str = str_replace(settings('string_strip'), '', $str);
    $str = callback($callback, $str);
    return $str;
}

/**
 * Build a slug string (for DOM IDs for instance) from an original string
 *
 * @param   string          $str        The original string
 * @param   null|callable   $callback   A callback method to finally transform the string
 * @setting slug_mask                   REGEX mask to match striped chars
 * @setting slug_glue                   Car. used to replace unwanted cars.
 * @return  string
 */
function slugify($str, $callback = null)
{
    $str = safe_string($str);
    $str = preg_replace(settings('slug_mask'), settings('slug_glue'), (string) $str);
    $str = strtolower(trim($str, settings('slug_glue')));
    $str = callback($callback, $str);
    return $str;
}

/**
 * Build a readable date-time from a timestamp or a `DateTime` object
 *
 * @param   float|DateTime  $str        The date or timestamp to transform
 * @param   string          $format     The format of date to use
 * @param   null|callable   $callback   A callback method to finally transform the string
 * @setting date_format                 Default value for `$format`
 * @return  string
 */
function datify($str, $format = null, $callback = null)
{
    if (is_null($format)) $format = settings('date_format');
    $str = (is_object($str) && ($str instanceof DateTime)) ? $str->format($format) : (is_numeric($str) ? date($format, $str) : safe_string($str));
    $str = callback($callback, $str);
    return $str;
}

/**
 * Escape a string
 *
 * Escape for a command line usage if `IS_CLI`.
 *
 * Escape parameter (GET or POST) otherwise.
 *
 * @param   string  $arg
 * @param   bool    $cmd    Define whether `$arg` is a command or not
 * @setting encoding
 * @return  string|array
 */
function escape($arg, $cmd = false)
{
    if (IS_CLI) {
        $result = ($cmd ? escapeshellcmd($arg) : escapeshellarg($arg));
    } else {
        if (is_string($arg)) {
            $result = stripslashes(htmlspecialchars($arg, ENT_COMPAT, settings('encoding')));
        } elseif (is_array($arg)) {
            $result = array();
            foreach($arg as $var=>$val) {
                $result[$var] = escape($val);
            }
        } else {
            $result = $arg;
        }
    }
    return $result;
}

// --------------------------------------------
// Translations methods
// --------------------------------------------

/**
 * Translator
 *
 *      gender treatment "[...;gender:f/m;...]string"
 *      number treatment "[...;number:s/p;...]string"
 *
 * @param   string      $str
 * @param   null/array  $args
 * @return  string
 */
function translate($str, array $args = null)
{
    $index      = $str;
    $lns        = settings('i18n');
    $user_ln    = settings('user_ln');
    $lns_def    = $lns['en'];
    $lns_user   = array_key_exists($user_ln, $lns) ? $lns[$user_ln] : $lns_def;
    $str        = array_key_exists($index, $lns_user) ? $lns_user[$index] : (
                    array_key_exists($index, $lns_def) ? $lns_def[$index] : $str
                );
    $data       = array();
    if (!empty($args)) {
        foreach ($args as $i=>$val) {
            $trans      = translate($val);
            list($trans_str, $trans_data) = get_language_string_info($trans);
            $data       = array_merge($data, $trans_data);
            $args[$i]   = $trans_str;
        }
        $prefixes = array();
        foreach ($data as $var=>$val) {
            $data_str   = "{$var}:{$val}";
            $prefixes[] = $data_str;
            $prefix     = "[{$data_str}]";
            $str        = array_key_exists($prefix.$index, $lns_user) ? $lns_user[$prefix.$index] : (
                            array_key_exists($prefix.$index, $lns_def) ? $lns_def[$prefix.$index] : $str
                        );
        }
        $global_prefix  = '['.join(';', $prefixes).']';
        $str            = array_key_exists($global_prefix.$index, $lns_user) ? $lns_user[$global_prefix.$index] : (
                            array_key_exists($global_prefix.$index, $lns_def) ? $lns_def[$global_prefix.$index] : $str
                        );
        array_unshift($args, $str);
        $str = @call_user_func_array('sprintf', $args);
    }
    return $str;
}

/**
 * Get the meta-data of a language string
 *
 * @param   string  $str
 * @return  array   array( (string) $str , (array) $info )
 */
function get_language_string_info($str)
{
    $info = array();
    if (0!==preg_match('~^\[([^\]]+)\]~i', $str, $matches)) {
        $str            = str_replace($matches[0], '', $str);
        $types          = $matches[1];
        $types_items    = explode(';', $types);
        foreach ($types_items as $item) {
            $parts      = explode(':', $item);
            if (count($parts)>1) {
                $info[$parts[0]]    = $parts[1];
            } else {
                $info[]             = $parts[0];
            }
        }
    }
    return array($str, $info);
}

// --------------------------------------------
// URLs methods
// --------------------------------------------

/**
 * Validate an URL (without a hash content)
 *
 * @param   string      $url The string to validate
 * @param   array       $protocols Table of Internet protocols to verify (by default : 'http', 'https', 'ftp')
 * @return  bool        Returns `true` if this is a URL in one of the specified protocols
 */
function is_url($url = null, $protocols = array('http','https','ftp'))
{
    if (is_null($url) || !$url || !is_string($url)) {
        return false;
    }
    return (bool) (preg_match("/^[".join('|', $protocols)."]+[:\/\/]+[A-Za-z0-9\-_]+(\\.)?+[A-Za-z0-9\.\/%&=\?\-_]+$/i", $url) > 0);
}

/**
 * @param   bool $use_forwarded_host
 * @return  null|string
 */
function get_current_url($use_forwarded_host = false)
{
    if (IS_CLI) return null;
    $s = $_SERVER;
    return get_base_url($s, $use_forwarded_host) . $s['REQUEST_URI'];
}

/**
 * @return null|string
 */
function get_current_uri()
{
    if (IS_CLI) return null;
    $s = $_SERVER;
    if (isset($s['REQUEST_URI'])) {
        return $s['REQUEST_URI'];
    } else {
        $request_uri = $s['PHP_SELF'];
        if ($s['QUERY_STRING'] && !strpos($s['REQUEST_URI'], '?')) {
            $request_uri .= '?' . $s['QUERY_STRING'];
        }
        return $request_uri;
    }
}

/**
 * @param   bool    $use_forwarded_host
 * @return  null|string
 */
function get_base_url($use_forwarded_host = false)
{
    if (IS_CLI) return null;
    $s      = $_SERVER;
    $ssl    = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
    $sp     = strtolower($s['SERVER_PROTOCOL']);
    $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
    $port   = $s['SERVER_PORT'];
    $port   = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
    $host   = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
    $host   = isset($host) ? $host : $s['SERVER_NAME'] . $port;
    return $protocol . '://' . $host;
}

// --------------------------------------------
// Views methods
// --------------------------------------------

/**
 * Get a view file content
 *
 * @param   string  $path
 * @param   array   $params
 * @return  string
 * @throws  InvalidArgumentException if the view file does not exist
 */
function get_view($path, array $params = array())
{
    if (!file_exists($path)) {
        throw new InvalidArgumentException(
            sprintf('View file "%s" not found!', $path)
        );
    }
    extract(settings());
    if (!empty($params)) {
        extract($params);
    }
    ob_start();
    require $path;
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}

// --------------------------------------------
// Initial settings
// --------------------------------------------

/**
 * Internal library info string
 *
 * @package php-quick-lib-internals
 * @return  void
 */
function php_quick_lib_info()
{
    output(
        PHPQLIB_SHORTKEY .' '. PHPQLIB_VERSION . settings('new_line')
        . 'Licensed under the Apache 2.0 license (http://www.apache.org/licenses/)' . settings('new_line')
        . 'Sources at ' . PHPQLIB_SOURCES
    );
}

/**
 * Are we in command-line environment
 */
define('IS_CLI', (bool) (strpos(php_sapi_name(),'cli')!==false));

// PHP default settings
@error_reporting(E_ALL | E_STRICT);
@ini_set('display_errors', 1);
$dtmz = @date_default_timezone_get();
@date_default_timezone_set($dtmz?:'UTC');
if (IS_CLI) {
    @ini_set('register_argc_argv', 1);
    @ini_set('html_errors', 0);
    if (function_exists('xdebug_disable')) {
        xdebug_disable();
    }
}

//ini_set('display_errors', 0);
//register_shutdown_function('shutdown_handler');
//set_error_handler('error_handler', E_ALL | E_STRICT);
//set_exception_handler('exception_handler');
//header_register_callback('header_handler');

// begin with default settings
if (!isset($phpqlib_defaults)) {
    $phpqlib_defaults = array(
        'content-type'      => 'text/html',
        'encoding'          => 'utf8',
        'string_spacify'    => array('-', '_'),
        'string_strip'      => array(),
        'slug_glue'         => '-',
        'slug_mask'         => '~[^a-zA-Z0-9]+~u',
        'date_format'       => 'd M Y H:i:s',
        'user_ln'           => 'en',
        'i18n'              => array( 'en' => array() ),
    );
}
settings($phpqlib_defaults);

// special loading of current working directory as a `cwd` setting
settings('cwd', getcwd());

// special initial setting for the "new_line" characters
settings('new_line', IS_CLI ? PHP_EOL : '<br />' . PHP_EOL);

// the CLI case
if (IS_CLI) {
    // for rare cases where I/O constants are not defined
    if (!defined('STDIN'))  define('STDIN',  fopen('php://stdin', 'r'));
    if (!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'w'));
    if (!defined('STDERR')) define('STDERR', fopen('php://stderr', 'a'));

    // special loading of $argv as a `argv` setting
    if (isset($argv)) settings('argv', $argv);

    // set content-type to simple plain text
    settings('content-type', 'text/plain');

}


// Endfile
// vim: autoindent tabstop=4 shiftwidth=4 expandtab softtabstop=4 filetype=php
