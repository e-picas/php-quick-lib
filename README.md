PHP-Quick-Library
=================

[![Build Status](https://travis-ci.org/piwi/php-quick-lib.svg?branch=dev)](https://travis-ci.org/piwi/php-quick-lib)
A short procedural library to help write PHP scripts quickly.

What's this?
------------

The `php-quick-lib.php` is a short library of PHP functions (all procedural) 
to help in writing PHP scripts quickly. It proposes some often used functions for 
web and shell scripts.

How to use it?
--------------

A starter template using the library could be something like:

```php
// include the library
if (file_exists($_library = __DIR__.'/php-quick-lib.php')) {
    require_once $_library;
} else {
    die("> ERROR !! - PHP-quick-library '$_library' not found!");
}

// load some settings in the library's registry
settings('option1', 'my value');
settings('option2', 'my value');

// let's go
extract(settings()); // this will extract all settings in current environment

...
```

Some simple methods can help building shell scripts:

```php
// custom script usage string
function usage($status = 0) {
    $argv = settings('argv');
    echo "usage: {$argv[0]} <option1> <option2>", PHP_EOL;
    exit($status);
}

// usage info if so
if (count($argv)>1 && in_array($argv[1], array('help', '-h', '--help')) && function_exists('usage')) {
    usage(); exit();
}
```

The library embeds a set of internal handlers designed for command-line usage. To use them, you may write:

```php
set_error_handler('error_handler');
set_exception_handler('exception_handler');
register_shutdown_function('shutdown_handler');
header_register_callback('header_handler');
```

It also embeds an internal registry system with a single method for simplicity:

```php
// this will add or override a "name" setting with value "value"
settings('name', 'value');

// this will return the "name" setting value
$val = settings('name');

// this will return the whole settings table
$options = settings();
```

For [Composer](http://getcomposer.org/) users
---------------------------------------------

The package is registered in [Packagist](http://packagist.org/) so you can add it to your project's requirements:

    "require": {
        ...
        "piwi/php-quick-lib": "1.*"
    }

The library is defined as a `binary` file so it should be linked in your project's `bin` directory if so. To include it,
just use:

```php
<?php
...
require "project/bin/path/php-quick-lib.php";
...
```
